import dao.*;
import entities.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import java.util.List;

/**
 *
 */
public class Main {

    private static SessionFactory sessionFactory = null;



    public static void main(String[] args) {

        sessionFactory = new Configuration().configure().buildSessionFactory();

        insertSomeData();
        showDataInDB();
        doSomeManipulations();
        showDataInDB();
    }


    /**
     * ф-ция добавляет некие данные
     */
    private static void insertSomeData(){

        Session session = sessionFactory.openSession();

        try {
            //создаем скилы
            SkillsDao skillsDao = new SkillsDao(session);

            Skills skillJS = new Skills("JavaScript");
            skillsDao.create(skillJS);
            Skills skillJava = new Skills("Java");
            skillsDao.create(skillJava);
            Skills skillPHP = new Skills("PHP");
            skillsDao.create(skillPHP);
            Skills skillHTML = new Skills("HTML");
            skillsDao.create(skillHTML);
            Skills skillCPlus = new Skills("C++");
            skillsDao.create(skillCPlus);

            //создаем проекты
            ProjectsDao projectsDao = new ProjectsDao(session);

            Projects project_1 = new Projects("BigData", 100, 25000);
            projectsDao.create(project_1);
            Projects project_2 = new Projects("TestSystem", 200, 43000);
            projectsDao.create(project_2);
            Projects project_3 = new Projects("AnalyticSystem", 350, 75000);
            projectsDao.create(project_3);
            Projects project_4 = new Projects("HealthSystem", 500, 90000);
            projectsDao.create(project_4);

            //создаем заказчиков
            CustomersDao customersDao = new CustomersDao(session);

            Customers customer_1 = new Customers("Ivan", "Petrov", "ivan@yo.com", "Kiev", "233-89-981");
            customer_1.addProject(project_1);
            customersDao.create(customer_1);
            Customers customer_2 = new Customers("Pavel", "Ivanov", "pavel@yo.com", "Odessa", "633-89-981");
            customer_2.addProject(project_2);
            customersDao.create(customer_2);
            Customers customer_3 = new Customers("Sergey", "Morozov", "sergey@yo.com", "Lviv", "943-89-981");
            customer_3.addProject(project_3);
            customersDao.create(customer_3);
            Customers customer_4 = new Customers("Sveta", "Neizvestnaya", "svets@yo.com", "Nikolayev", "733-89-981");
            customer_4.addProject(project_4);
            customersDao.create(customer_4);

            //создаем компании
            CompaniesDao companiesDao = new CompaniesDao(session);

            Companies company_1 = new Companies("ABC", "90132662", "454-728-902", "Kiev");
            company_1.addProject(project_1);
            companiesDao.create(company_1);
            Companies company_2 = new Companies("Cloud-company", "50142662", "418-728-902", "Lviv");
            company_2.addProject(project_2);
            companiesDao.create(company_2);
            Companies company_3 = new Companies("EngagePoint", "10162662", "498-758-902", "Kiev");
            company_3.addProject(project_3);
            companiesDao.create(company_3);
            Companies company_4 = new Companies("Google", "80132662", "154-728-902", "Kiev");
            company_4.addProject(project_4);
            companiesDao.create(company_4);

            //добавляем разработчиков
            DevelopersDao developersDao = new DevelopersDao(session);

            Developers developer_1 = new Developers("Alex", "alex2014", "213-123-32", 700);
            developer_1.addSkill(skillsDao.getOneByName("JavaScript"));
            developer_1.addSkill(skillHTML);
            developer_1.addProject(project_1);
            developersDao.create(developer_1);
            Developers developer_2 = new Developers("Max", "max2010", "313-123-32", 800);
            developer_2.addSkill(skillsDao.getOneByName("JavaScript"));
            developer_2.addSkill(skillPHP);
            developer_2.addProject(project_2);
            developersDao.create(developer_2);
            Developers developer_3 = new Developers("Nikolay", "niko2011", "413-123-32", 1300);
            developer_3.addSkill(skillJava);
            developer_3.addSkill(skillHTML);
            developer_3.addProject(project_3);
            developersDao.create(developer_3);
            Developers developer_4 = new Developers("Stas", "stas2011", "713-123-32", 2300);
            developer_4.addSkill(skillCPlus);
            developer_4.addSkill(skillsDao.getOneByName("Java"));
            developer_4.addProject(project_4);
            developersDao.create(developer_4);
            Developers developer_5 = new Developers("Petr", "petr_new_2014", "2813-123-32", 800);
            developer_5.addSkill(skillsDao.getOneByName("JavaScript"));
            developer_5.addSkill(skillHTML);
            developer_5.addProject(project_4);
            developersDao.create(developer_5);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(session != null && session.isOpen()){
                session.close();
            }
        }
    }



    /**
     * отображает содержимое базы
     */
    private static void showDataInDB(){

        Session session = sessionFactory.openSession();

        try {
            //отображаем список навыков
            SkillsDao skillsDao = new SkillsDao(session);
            List<Skills> skillsList = skillsDao.getAll();
            System.out.println("==========   List of skills   ============");
            for (Skills skill : skillsList) {
                System.out.println(skill);
            }
            //Получаем список всех разработчиков
            DevelopersDao developersDao = new DevelopersDao(session);
            List<Developers> devList = developersDao.getAll();
            System.out.println("==========   List of developers   ============");
            for (Developers developer : devList) {
                System.out.println(developer);
            }
            //Получаем список всех заказчиков
            CustomersDao customersDao = new CustomersDao(session);
            List<Customers> customersList = customersDao.getAll();
            System.out.println("==========   List of customers   ============");
            for (Customers customer : customersList) {
                System.out.println(customer);
            }
            //Получаем список всех компаний
            CompaniesDao companiesDao = new CompaniesDao(session);
            List<Companies> companiesList = companiesDao.getAll();
            System.out.println("==========   List of companies   ============");
            for (Companies company : companiesList) {
                System.out.println(company);
            }
            //Получаем список всех проектов
            ProjectsDao projectsDao = new ProjectsDao(session);
            List<Projects> projectsList = projectsDao.getAll();
            System.out.println("==========   List of projects   ============");
            for (Projects project : projectsList) {
                System.out.println(project);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            if(session != null && session.isOpen()){
                session.close();
            }
        }
    }


    /**
     * выполняет некие манипуляции с данными в базе
     */
    private static void doSomeManipulations(){

        Session session = sessionFactory.openSession();

        try {
            DevelopersDao developersDao = new DevelopersDao(session);
            Developers devMax  = developersDao.getOneByName("Max");
            if(devMax != null){
                List<Projects> devMaxProjects = devMax.getProjectsList();
                Developers devStas  = developersDao.getOneByName("Stas");
                if(devStas != null){
                    for(Projects project: devMaxProjects){
                        devStas.addProject(project);
                    }
                    developersDao.update(devStas);
                    developersDao.delete(devMax);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            if(session != null && session.isOpen()){
                session.close();
            }
        }
    }



}
