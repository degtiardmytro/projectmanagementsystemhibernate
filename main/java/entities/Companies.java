package entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 *
 */
@Entity
public class Companies{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_companies", nullable = false, unique = true, length = 11)
    private int id;

    @Column(length = 45, nullable = false)
    private String name;

    @Column(length = 20, nullable = false, name = "registration_number")
    private String registrationNumber;

    @Column(length = 15)
    private String phone;

    private String address;


    @ManyToMany
    private Set<Projects> projectsSet = new HashSet<Projects>();

    public Companies() {}

    public Companies(String name, String registrationNumber, String phone, String address) {
        this.name = name;
        this.registrationNumber = registrationNumber;
        this.phone = phone;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<Projects> getProjectsSet() {
        return projectsSet;
    }

    public void setProjectsSet(Set<Projects> projectsSet) {
        this.projectsSet = projectsSet;
    }

    public void addProject(Projects project){

        if(projectsSet != null){
            projectsSet.add(project);
        }
    }

    @Override
    public String toString() {

        StringBuilder projStr = new StringBuilder();
        if(projectsSet != null) {
            Iterator<Projects> projectIterator = projectsSet.iterator();
            while (projectIterator.hasNext()) {
                projStr.append(projectIterator.next().getName());
                if (projectIterator.hasNext()) {
                    projStr.append(", ");
                }
            }
        }

        return String.format("Companies{id=%d, name='%s', registrationNumber='%s', phone='%s', address='%s', projects=[%s]}",
                id,
                name,
                registrationNumber,
                phone,
                address,
                projStr
        );
    }
}
