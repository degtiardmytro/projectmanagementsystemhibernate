package entities;

import dao.BaseDao;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 */
@Entity
public class Skills{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_skills", nullable = false, unique = true, length = 11)
    private int id;

    private String name;

    @ManyToMany(mappedBy = "skillsList")
    private List<Developers> developersList = new ArrayList<Developers>();

    public Skills() {}

    public Skills(String name) {
        this.name = name;
        this.developersList = new ArrayList<Developers>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**/
    public List<Developers> getDevelopersList() {
        return developersList;
    }

    public void setDevelopersList(List<Developers> developersList) {
        this.developersList = developersList;
    }

    @Override
    public String toString() {

        StringBuilder devStr = new StringBuilder();
        if(developersList != null) {
            Iterator<Developers> developersIterator = developersList.iterator();
            while (developersIterator.hasNext()) {
                devStr.append(developersIterator.next().getName());
                if (developersIterator.hasNext()) {
                    devStr.append(", ");
                }
            }
        }
        return String.format("Skills{id=%d, name='%s', developers=[%s]}", id, name, devStr);
    }
}
