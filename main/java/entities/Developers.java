package entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Entity
public class Developers{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_developers", nullable = false, unique = true, length = 11)
    private int id;

    @Column(length = 45, nullable = false)
    private String name;

    @Column(length = 45)
    private String skype;

    private String phone;

    @Column(nullable = false)
    private float salary;

    @ManyToMany
    private List<Skills> skillsList = new ArrayList<Skills>();

    @ManyToMany
    private List<Projects> projectsList = new ArrayList<Projects>();

    public Developers() {}

    public Developers(String name, String skype, String phone, float salary) {
        this.name = name;
        this.skype = skype;
        this.phone = phone;
        this.salary = salary;
        this.skillsList = new ArrayList<Skills>();
        this.projectsList = new ArrayList<Projects>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public List<Skills> getSkillsList() {
        return skillsList;
    }

    public void setSkillsList(List<Skills> skillsList) {
        this.skillsList = skillsList;
    }

    public List<Projects> getProjectsList() {
        return projectsList;
    }

    public void setProjectsList(List<Projects> projectsList) {
        this.projectsList = projectsList;
    }

    public void addSkill(Skills skill){
        if(skillsList != null){
            skillsList.add(skill);
        }
    }

    public void addProject(Projects project){
        if(projectsList != null){
            projectsList.add(project);
        }
    }

    @Override
    public String toString() {
        StringBuilder skillStr = new StringBuilder();
        if(skillsList != null) {
            Iterator<Skills> skillsIterator = skillsList.iterator();
            while (skillsIterator.hasNext()) {
                skillStr.append(skillsIterator.next().getName());
                if (skillsIterator.hasNext()) {
                    skillStr.append(", ");
                }
            }
        }

        StringBuilder projStr = new StringBuilder();
        if(projectsList != null) {
            Iterator<Projects> projectIterator = projectsList.iterator();
            while (projectIterator.hasNext()) {
                projStr.append(projectIterator.next().getName());
                if (projectIterator.hasNext()) {
                    projStr.append(", ");
                }
            }
        }

        return String.format("Developers{id=%d, name='%s', skype='%s', phone='%s', salary=%f, skills=[%s], projects=[%s]}",
                id,
                name,
                skype,
                phone,
                salary,
                skillStr,
                projStr
        );
    }
}
