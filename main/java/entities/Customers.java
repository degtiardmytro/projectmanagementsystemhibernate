package entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 */
@Entity
public class Customers{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_customers", nullable = false,unique = true, length = 11)
    private int id;

    @Column(length = 45)
    private String name;

    @Column(length = 45)
    private String surname;

    @Column(length = 100)
    private String email;

    private String address;

    @Column(length = 15)
    private String phone;

    @ManyToMany
    private Set<Projects> projectsSet = new HashSet<Projects>();

    public Customers() {}

    public Customers(String name, String surname, String email, String address, String phone) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.address = address;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Set<Projects> getProjectsSet() {
        return projectsSet;
    }

    public void setProjectsSet(Set<Projects> projectsSet) {
        this.projectsSet = projectsSet;
    }

    public void addProject(Projects project){
        if(projectsSet != null){
            projectsSet.add(project);
        }
    }

    @Override
    public String toString() {

        StringBuilder projStr = new StringBuilder();
        if(projectsSet != null) {
            Iterator<Projects> projectIterator = projectsSet.iterator();
            while (projectIterator.hasNext()) {
                projStr.append(projectIterator.next().getName());
                if (projectIterator.hasNext()) {
                    projStr.append(", ");
                }
            }
        }

        return String.format("Customers{id=%d, name='%s', surname='%s', email='%s', address='%s', phone=[%s], projects=[%s]}",
                id,
                name,
                surname,
                email,
                address,
                phone,
                projStr
        );
    }
}
