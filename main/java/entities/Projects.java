package entities;

import javax.persistence.*;
import java.util.*;

/**
 *
 */
@Entity
public class Projects{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_projects", nullable = false, unique = true, length = 11)
    private int id;

    @Column(nullable = false)
    private String name;

    private int deadline;

    private float cost;

    @ManyToMany(mappedBy = "projectsList")
    private List<Developers> developersList = new ArrayList<Developers>();

    @ManyToMany(mappedBy = "projectsSet")
    private Set<Companies> companiesSet = new HashSet<Companies>();

    @ManyToMany(mappedBy = "projectsSet")
    private Set<Customers> customersSet = new HashSet<Customers>();

    public Projects() {}

    public Projects(String name, int deadline, float cost) {
        this.name = name;
        this.deadline = deadline;
        this.cost = cost;
        this.developersList = new ArrayList<Developers>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDeadline() {
        return deadline;
    }

    public void setDeadline(int deadline) {
        this.deadline = deadline;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    /**/
    public List<Developers> getDevelopersList() {
        return developersList;
    }

    public void setDevelopersList(List<Developers> developersList) {
        this.developersList = developersList;
    }

    public void addDeveloper(Developers developer){
        if(developersList != null){
            developersList.add(developer);
        }
    }

    public Set<Companies> getCompaniesSet() {
        return companiesSet;
    }

    public void setCompaniesSet(Set<Companies> companiesSet) {
        this.companiesSet = companiesSet;
    }

    @Override
    public String toString() {

        //список разработчиков
        StringBuilder developerStr = new StringBuilder();
        if(developersList != null) {
            Iterator<Developers> developersIterator = developersList.iterator();
            while (developersIterator.hasNext()) {
                developerStr.append(developersIterator.next().getName());
                if (developersIterator.hasNext()) {
                    developerStr.append(", ");
                }
            }
        }

        //список компаний
        StringBuilder companyStr = new StringBuilder();
        if(companiesSet != null) {
            Iterator<Companies> companiesIterator = companiesSet.iterator();
            while (companiesIterator.hasNext()) {
                companyStr.append(companiesIterator.next().getName());
                if (companiesIterator.hasNext()) {
                    companyStr.append(", ");
                }
            }
        }

        //список заказчиков
        StringBuilder customerStr = new StringBuilder();
        if(customersSet != null) {
            Iterator<Customers> customersIterator = customersSet.iterator();
            while (customersIterator.hasNext()) {
                customerStr.append(customersIterator.next().getName());
                if (customersIterator.hasNext()) {
                    customerStr.append(", ");
                }
            }
        }

        return String.format("Projects{id=%d, name='%s', deadline=%d, cost=%f, developers=[%s], companies=[%s], customers=[%s]}",
                id,
                name,
                deadline,
                cost,
                developerStr,
                companyStr,
                customerStr
        );
    }
}
