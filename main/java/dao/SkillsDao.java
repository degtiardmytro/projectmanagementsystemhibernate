package dao;


import entities.Skills;
import org.hibernate.Session;


/**
 *
 */
public class SkillsDao extends BaseDao<Skills> {



    public SkillsDao(Session session) {
        super(session);
        GET_ALL_HQL = "From Skills";
        DELETE_BY_ID_HQL = "DELETE FROM Skills WHERE id_skills = :id";
        GET_ONE_BY_ID_HQL =  "FROM Skills WHERE id_skills = :id";
        GET_BY_NAME_HQL = "FROM Skills WHERE name = :name";
    }




}
