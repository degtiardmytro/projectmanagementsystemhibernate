package dao;

import entities.Companies;
import org.hibernate.Session;


/**
 *
 */
public class CompaniesDao extends BaseDao<Companies> {

    public CompaniesDao(Session session) {
        super(session);
        GET_ALL_HQL = "From Companies";
        DELETE_BY_ID_HQL = "DELETE FROM Companies WHERE id_companies = :id";
        GET_ONE_BY_ID_HQL =  "FROM Companies WHERE id_companies = :id";
        GET_BY_NAME_HQL = "FROM Companies WHERE name = :name";
    }
}
