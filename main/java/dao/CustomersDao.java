package dao;

import entities.Customers;
import org.hibernate.Session;

/**
 *
 */
public class CustomersDao extends BaseDao<Customers> {

    public CustomersDao(Session session) {
        super(session);
        GET_ALL_HQL = "From Customers";
        DELETE_BY_ID_HQL = "DELETE FROM Customers WHERE id_customers = :id";
        GET_ONE_BY_ID_HQL =  "FROM Customers WHERE id_customers = :id";
        GET_BY_NAME_HQL = "FROM Customers WHERE name = :name";
    }
}
