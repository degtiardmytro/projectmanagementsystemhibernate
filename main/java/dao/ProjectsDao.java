package dao;

import entities.Projects;
import org.hibernate.Session;


/**
 *
 */
public class ProjectsDao extends BaseDao<Projects> {

    public ProjectsDao(Session session) {
        super(session);
        GET_ALL_HQL = "From Projects";
        DELETE_BY_ID_HQL = "DELETE FROM Projects WHERE id_projects = :id";
        GET_ONE_BY_ID_HQL =  "FROM Projects WHERE id_projects = :id";
        GET_BY_NAME_HQL = "FROM Projects WHERE name = :name";
    }

}
