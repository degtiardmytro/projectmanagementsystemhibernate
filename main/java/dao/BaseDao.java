package dao;


import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 *
 */
public abstract class BaseDao<T> {

    protected static Logger LOG = null;
    protected Session session;

    String GET_ALL_HQL = "";
    String DELETE_BY_ID_HQL = "";
    String GET_ONE_BY_ID_HQL = "";
    String GET_BY_NAME_HQL = "";

    static {
        LOG = Logger.getLogger(BaseDao.class.getName());
    }

    BaseDao(Session session) {
        this.session = session;
    }


    /**
     * добавляем обьект в БД
     * @param entity - object that can saved to DB
     */
    public void create(T entity){

        if(!session.isOpen()){
            return;
        }
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            session.save(entity);
            transaction.commit();
        }catch (Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            LOG.info(e.getMessage());
        }
    }


    /**
     * обновляем обьект в БД
     * @param entity - object that can saved to DB
     */
    public void update(T entity){

        if(!session.isOpen()){
            return;
        }
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            session.update(entity);
            transaction.commit();
        }catch (Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            LOG.info(e.getMessage());
        }
    }


    /**
     * получаем обьект из БД
     * @param id -
     */
    public T read(Integer id){

        if(!session.isOpen()){
            return null;
        }
        Transaction transaction = null;
        T obj = null;
        try{
            transaction = session.beginTransaction();
            Query query = session.createQuery(GET_ONE_BY_ID_HQL);
            query.setParameter("id", id);
            obj = (T)query.uniqueResult();
            transaction.commit();
        }catch (Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            LOG.info(e.getMessage());
        }
        return obj;
    }



    /**
     * удаляем обьект из БД
     * @param entity - object that can saved to DB
     */
    public void delete(T entity){

        if(!session.isOpen()){
            return;
        }
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            session.delete(entity);
            transaction.commit();
        }catch (Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            LOG.info(e.getMessage());
        }
    }

    /**
     * удаляем обьект из БД по его id
     * @param id -
     */
    public void delete(Integer id){

        if(!session.isOpen()){
            return;
        }
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            Query query = session.createQuery(DELETE_BY_ID_HQL);
            query.setParameter("id", id);
            query.executeUpdate();
            transaction.commit();
        }catch (Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            LOG.info(e.getMessage());
        }
    }

    /**
     * получаем список сущностей
     * @return -
     */
    public List<T> getAll(){

        if(!session.isOpen()){
            return null;
        }
        Transaction transaction = null;
        List<T> list = null;
        try{
            transaction = session.beginTransaction();
            Query query = session.createQuery(GET_ALL_HQL);
            list = query.list();
            transaction.commit();
        }catch (Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            LOG.info(e.getMessage());
        }
        return list;
    }


    /**
     * получаем обьект из БД по имени
     * @param name -
     */
    public T getOneByName(String name){

        if(!session.isOpen()){
            return null;
        }
        Transaction transaction = null;
        T obj = null;
        try{
            transaction = session.beginTransaction();
            Query query = session.createQuery(GET_BY_NAME_HQL);
            query.setParameter("name", name);
            obj = (T)query.uniqueResult();
            transaction.commit();
        }catch (Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            LOG.info(e.getMessage());
        }
        return obj;
    }


    /**
     * получаем список сущностей  по имени
     * @return -
     */
    public List<T> getAllByName(String name){

        if(!session.isOpen()){
            return null;
        }
        Transaction transaction = null;
        List<T> list = null;
        try{
            transaction = session.beginTransaction();
            Query query = session.createQuery(GET_BY_NAME_HQL);
            query.setParameter("name", name);
            list = query.list();
            transaction.commit();
        }catch (Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            LOG.info(e.getMessage());
        }
        return list;
    }


}
