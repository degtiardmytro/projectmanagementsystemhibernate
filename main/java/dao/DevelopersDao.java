package dao;


import entities.Developers;
import org.hibernate.Session;


/**
 *
 */
public class DevelopersDao extends BaseDao<Developers> {


    public DevelopersDao(Session session) {
        super(session);
        GET_ALL_HQL = "From Developers";
        DELETE_BY_ID_HQL = "DELETE FROM Developers WHERE id_developers = :id";
        GET_ONE_BY_ID_HQL =  "FROM Developers WHERE id_developers = :id";
        GET_BY_NAME_HQL = "FROM Developers WHERE name = :name";
    }






}
